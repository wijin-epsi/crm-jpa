package fr.wijin.crm.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.model.User;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Persistence;

@TestMethodOrder(OrderAnnotation.class)
class UserDaoImplTest {

	Logger logger = LoggerFactory.getLogger(UserDaoImplTest.class);

	private static EntityManager em;
	private static UserDaoImpl userDao;

	@BeforeAll
	static void setup() {
		Map<String, String> properties = new HashMap<>();
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.format_sql", "true");
		// Utilisation du "persistence unit" de test (BDD H2)
		em = Persistence.createEntityManagerFactory("crm-test", properties).createEntityManager();
		userDao = new UserDaoImpl(em);
	}

	@Test
	@Order(1)
	void givenUsernameAndPassword_whenCallingTypedQueryMethod_thenReturnUser() {
		User user = userDao.getByUsernameAndPassword("toto", "1234");
		Assertions.assertNotNull(user, "No user found for username and password");
	}

	@Test
	@Order(2)
	void givenId_whenCallingTypedQueryMethod_thenReturnUser() {
		User user = userDao.getById(1);
		Assertions.assertNotNull(user, "No user found for id 1");
	}

	@Test
	@Order(3)
	void whenCallingTypedQueryMethod_thenReturnListOfUsers() {
		List<User> users = userDao.getAll();
		Assertions.assertEquals(1, users.size(), "Wrong number of users");
	}

	@Test
	@Order(4)
	void givenUser_whenCallingTypedQueryMethodForCreate_thenReturnOK() {
		User newUser = new User();
		newUser.setUsername("mtest");
		newUser.setPassword("mtest");
		newUser.setMail("mtest@test.fr");

		List<User> users = userDao.getAll();
		int numberOfUsersBeforeCreation = users.size();

		try {
			em.getTransaction().begin();
			userDao.createUser(newUser);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			Assertions.fail();
		}

		List<User> usersAfterCreation = userDao.getAll();
		int numberOfUsersAfterCreation = usersAfterCreation.size();
		Assertions.assertEquals(numberOfUsersBeforeCreation + 1, numberOfUsersAfterCreation);
	}

	@Test
	@Order(5)
	void givenUser_whenCallingTypedQueryMethodForUpdate_thenReturnOK() {

		User user = userDao.getById(1);
		user.setMail("nouveauMail@test.fr");

		try {
			em.getTransaction().begin();
			em.detach(user);
			userDao.updateUser(user);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			Assertions.fail();
		}

		User updatedUser = userDao.getById(1);
		Assertions.assertEquals("nouveauMail@test.fr", updatedUser.getMail());
	}

	@Test
	@Order(6)
	void givenUser_whenCallingTypedQueryMethodForDelete_thenReturnOK() {
		User user = userDao.getById(1);

		try {
			em.getTransaction().begin();
			userDao.deleteUser(user);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			Assertions.fail();
		}

		User deletedUser = userDao.getById(1);
		logger.debug("Deleted User = {}", deletedUser);
		Assertions.assertNull(deletedUser, "Deleted order must be null");
	}

	@Test
	@Order(7)
	void givenUser_whenCallingTypedQueryMethodForDelete_thenReturnKO() {
		User user = null;
		Assertions.assertThrows(DaoException.class, () -> {
			userDao.deleteUser(user);
		});

	}

}
