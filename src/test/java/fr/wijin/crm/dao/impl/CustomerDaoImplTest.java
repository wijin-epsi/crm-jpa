package fr.wijin.crm.dao.impl;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.model.Customer;
import fr.wijin.crm.model.Order;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Persistence;

@TestMethodOrder(OrderAnnotation.class)
class CustomerDaoImplTest {

	Logger logger = LoggerFactory.getLogger(CustomerDaoImplTest.class);

	public static final String LASTNAME = "JONES";

	private static EntityManager em;
	private static CustomerDaoImpl customerDao;
	private static OrderDaoImpl orderDao;

	@BeforeAll
	static void setup() {
		Map<String, String> properties = new HashMap<>();
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.format_sql", "true");
		// Utilisation du "persistence unit" de test (BDD H2)
		em = Persistence.createEntityManagerFactory("crm-test", properties).createEntityManager();
		customerDao = new CustomerDaoImpl(em);
		orderDao = new OrderDaoImpl(em);
	}

	@Test
	@org.junit.jupiter.api.Order(1)
	void givenCustomerId_whenCallingTypedQueryMethod_thenReturnCustomer() {
		logger.debug("*** givenCustomerId_whenCallingTypedQueryMethod_thenReturnCustomer");
		Customer customer = customerDao.getCustomerById(1);
		logger.debug("Customer = {}", customer);
		Assertions.assertNotNull(customer, "Customer not found");
	}

	@Test
	@org.junit.jupiter.api.Order(2)
	void givenCustomerId_whenCallingTypedQueryMethod_thenReturnExpectedCustomer() {
		Customer customer = customerDao.getCustomerById(1);
		Assertions.assertEquals(LASTNAME, customer.getLastname(), "Customer lastname should be " + LASTNAME);
	}

	@Test
	@org.junit.jupiter.api.Order(3)
	void givenCustomerId_whenCallingTypedQueryMethod_thenReturnExpectedNumberOfOrders() {
		Customer customer = customerDao.getCustomerById(1);
		Assertions.assertEquals(2, customer.getOrders().size(), "Number of orders should be " + 2);
	}

	@Test
	@org.junit.jupiter.api.Order(4)
	void givenCustomerLastname_whenCallingTypedQueryMethod_thenReturnCustomer() {
		Customer customer = customerDao.getCustomerByLastname(LASTNAME);
		Assertions.assertNotNull(customer, "Customer not found");
	}

	@Test
	@org.junit.jupiter.api.Order(5)
	void givenCustomerLastname_whenCallingTypedQueryMethod_thenReturnExpectedCustomer() {
		Customer customer = customerDao.getCustomerByLastname(LASTNAME);
		Assertions.assertEquals(1, customer.getId(), "Customer id should be 1");
	}

	@Test
	@org.junit.jupiter.api.Order(6)
	void whenCallingTypedQueryMethod_thenReturnListOfCustomers() {
		List<Customer> customers = customerDao.getAllCustomers();
		Assertions.assertEquals(4, customers.size(), "Wrong number of customers");
	}

	@Test
	@org.junit.jupiter.api.Order(7)
	void whenCallingNamedQueryMethod_thenReturnListOfInactiveCustomers() {
		List<Customer> customers = customerDao.getCustomersByActive(false);
		Assertions.assertEquals(2, customers.size(), "Wrong number of inactive customers");
	}

	@Test
	@org.junit.jupiter.api.Order(8)
	void whenCallingNativeQueryMethod_thenReturnListOfCustomers() {
		List<Customer> customers = customerDao.getCustomersWithMobile();
		Assertions.assertEquals(3, customers.size(), "Wrong number of customers with a mobile");
	}

	@Test
	@org.junit.jupiter.api.Order(9)
	void whenCallingCriteriaQueryMethod_thenReturnCustomer() {
		Customer customer = customerDao.getCustomerByIdWithCriteriaQuery(1);
		Assertions.assertEquals(LASTNAME, customer.getLastname(), "Customer lastname should be " + LASTNAME);
	}

	@Test
	@org.junit.jupiter.api.Order(10)
	void whenCallingCriteriaQueryMethod_thenReturnCustomersWithMobile() {
		List<Customer> customers = customerDao.getCustomersWithMobileWithCriteriaQuery();
		Assertions.assertEquals(3, customers.size(), "Wrong number of customers with a mobile");
	}

	@Test
	@org.junit.jupiter.api.Order(11)
	void givenCustomer_whenCallingTypedQueryMethodForCreate_thenReturnOK() {
		Customer newCustomer = new Customer();
		newCustomer.setFirstname("Winnie");
		newCustomer.setLastname("L'Ourson");
		newCustomer.setCompany("Disney");
		newCustomer.setPhone("0222222222");
		newCustomer.setMobile("0666666666");
		newCustomer.setMail("winnie.l.ourson@disney.com");
		newCustomer.setNotes("Les notes de Winnie");
		newCustomer.setActive(true);

		try {
			em.getTransaction().begin();
			customerDao.createCustomer(newCustomer);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Erreur : " + e.getMessage());
			e.printStackTrace();
			em.getTransaction().rollback();
			fail();
		}

		Customer customer = customerDao.getCustomerByLastname("L'Ourson");
		Assertions.assertNotNull(customer, "Winnie not found");
	}

	@Test
	@org.junit.jupiter.api.Order(12)
	void givenCustomer_whenCallingTypedQueryMethodForUpdate_thenReturnOK() {

		Customer customer = customerDao.getCustomerById(1);
		customer.setCompany("Nouvelle entreprise");

		try {
			em.getTransaction().begin();
			customerDao.updateCustomer(customer);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			fail();
		}

		Customer updatedCustomer = customerDao.getCustomerById(1);
		Assertions.assertEquals("Nouvelle entreprise", updatedCustomer.getCompany());
	}

	@Test
	@org.junit.jupiter.api.Order(13)
	void givenCustomer_whenCallingTypedQueryMethodForDelete_thenReturnOK() {
		Customer customer = customerDao.getCustomerById(2);

		try {
			em.getTransaction().begin();
			customerDao.deleteCustomer(customer);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			fail(e.getMessage());
		}

		Customer deletedCustomer = customerDao.getCustomerById(2);
		logger.debug("Deleted Customer = {}", deletedCustomer);
		Assertions.assertNull(deletedCustomer, "Deleted customer must be null");
		List<Order> orders = orderDao.getOrdersByCustomer(2);
		Assertions.assertEquals(0, orders.size(), "Deleted customer must be null");
	}

	@Test
	@org.junit.jupiter.api.Order(14)
	void test() {
		Customer customer = new Customer();
		customer.setId(1);
		try {
			customerDao.createCustomer(customer);
			fail("Should not succeed !");
		} catch (DaoException e) {
			logger.debug("OK : could not create a customer that already exists !!!");
		}
	}

}
