package fr.wijin.crm.dao.impl;

import java.util.List;

import fr.wijin.crm.dao.OrderDao;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.model.Order;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

public class OrderDaoImpl implements OrderDao {

	private EntityManager em;

	public OrderDaoImpl(EntityManager entityManager) {
		this.em = entityManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Order getById(Integer id) throws DaoException {
		try {
			return em.find(Order.class, id);
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Order> getAllOrders() throws DaoException {
		try {
			TypedQuery<Order> query = em.createQuery("SELECT o FROM Order o ORDER BY o.id", Order.class);
			return query.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Utilisation d'une NamedQuery (cf. classe Order)
	 */
	public List<Order> getOrdersByTypeAndStatus(String type, String status) throws DaoException {
		try {
			Query namedQuery = em.createNamedQuery("Order.findByTypeAndStatus");
			namedQuery.setParameter("type", type);
			namedQuery.setParameter("status", status);
			return (List<Order>) namedQuery.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Utilisation d'une NativeQuery
	 */
	public List<Order> getOrdersForNumberOfDays(Double numberOfDays) throws DaoException {
		try {
			Query nativeQuery = em.createNativeQuery("SELECT * FROM orders WHERE number_of_days > :numberOfDays",
					Order.class);
			nativeQuery.setParameter("numberOfDays", numberOfDays);
			return (List<Order>) nativeQuery.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Requête avec Criteria
	 */
	@Override
	public List<Order> getOrdersByCustomer(Integer customerId) throws DaoException {
		try {
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Order> criteriaQuery = criteriaBuilder.createQuery(Order.class);

			Root<Order> orderRoot = criteriaQuery.from(Order.class);

			criteriaQuery.select(orderRoot).where(criteriaBuilder.equal(orderRoot.get("customer"), customerId));

			TypedQuery<Order> query = em.createQuery(criteriaQuery);
			return query.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createOrder(Order order) throws DaoException {
		try {
			em.persist(order);
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateOrder(Order order) throws DaoException {
		try {
			Order orderToUpdate = em.merge(order);
			orderToUpdate.setLabel(order.getLabel());
			orderToUpdate.setNumberOfDays(order.getNumberOfDays());
			orderToUpdate.setAdrEt(order.getAdrEt());
			orderToUpdate.setTva(order.getTva());
			orderToUpdate.setStatus(order.getStatus());
			orderToUpdate.setType(order.getType());
			orderToUpdate.setNotes(order.getNotes());
			orderToUpdate.setCustomer(order.getCustomer());
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * With criteria update
	 */
	public void updateOrderStatus(String newStatus, String oldStatus, String type) throws DaoException {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaUpdate<Order> criteriaUpdate = criteriaBuilder.createCriteriaUpdate(Order.class);
		Root<Order> root = criteriaUpdate.from(Order.class);
		criteriaUpdate.set("status", newStatus);

		Predicate oldStatusPredicate = criteriaBuilder.equal(root.get("status"), oldStatus);
		Predicate typePredicate = criteriaBuilder.equal(root.get("type"), type);

		criteriaUpdate.where(criteriaBuilder.and(oldStatusPredicate, typePredicate));
		em.createQuery(criteriaUpdate).executeUpdate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteOrder(Order order) throws DaoException {
		try {
			em.remove(em.merge(order));
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

}
