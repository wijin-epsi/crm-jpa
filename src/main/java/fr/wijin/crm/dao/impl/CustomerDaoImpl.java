package fr.wijin.crm.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.model.Customer;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

public class CustomerDaoImpl implements CustomerDao {

	Logger logger = LoggerFactory.getLogger(CustomerDaoImpl.class);

	private EntityManager em;

	public CustomerDaoImpl(EntityManager entityManager) {
		this.em = entityManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Customer> getAllCustomers() throws DaoException {
		logger.info("Récupération des clients");
		try {
			TypedQuery<Customer> query = em.createQuery("SELECT c FROM Customer c ORDER BY c.id", Customer.class);
			return query.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Utilisation d'une NamedQuery (cf. classe Customer)
	 */
	public List<Customer> getCustomersByActive(Boolean active) throws DaoException {
		logger.debug("Récupération des clients avec active = {}", active);
		try {
			Query namedQuery = em.createNamedQuery("Customer.findByActive");
			namedQuery.setParameter("active", active);
			return (List<Customer>) namedQuery.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Utilisation d'une NativeQuery
	 */
	@SuppressWarnings("unchecked")
	public List<Customer> getCustomersWithMobile() throws DaoException {
		try {
			Query nativeQuery = em.createNativeQuery("SELECT * FROM customers WHERE mobile IS NOT NULL",
					Customer.class);
			return nativeQuery.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Utilisation d'une Criteria Query
	 */
	public Customer getCustomerByIdWithCriteriaQuery(Integer id) throws DaoException {
		try {
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Customer> criteriaQuery = criteriaBuilder.createQuery(Customer.class);

			Root<Customer> customerRoot = criteriaQuery.from(Customer.class);
			// ParameterExpression<Integer> parameterExpression =
			// criteriaBuilder.parameter(Integer.class);
			criteriaQuery.select(customerRoot).where(criteriaBuilder.equal(customerRoot.get("id"), id));
			// criteriaQuery.select(customerRoot).where(criteriaBuilder.equal(customerRoot.get("id"),
			// parameterExpression));

			TypedQuery<Customer> query = em.createQuery(criteriaQuery);
			// query.setParameter(parameterExpression, id);
			return query.getSingleResult();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Utilisation d'une Criteria Query
	 */
	public List<Customer> getCustomersWithMobileWithCriteriaQuery() throws DaoException {
		try {
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Customer> criteriaQuery = criteriaBuilder.createQuery(Customer.class);

			Root<Customer> customerRoot = criteriaQuery.from(Customer.class);

			criteriaQuery.select(customerRoot).where(criteriaBuilder.isNotNull(customerRoot.get("mobile")));

			TypedQuery<Customer> query = em.createQuery(criteriaQuery);
			return query.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer getCustomerByLastname(String lastname) throws DaoException {
		try {
			// TypedQuery<Customer> query = em.createQuery("SELECT c FROM Customer c WHERE
			// c.lastname = ?1",
			// Customer.class);
			TypedQuery<Customer> query = em.createQuery("SELECT c FROM Customer c WHERE c.lastname = :lastname",
					Customer.class);
			// query.setParameter(1, lastname);
			query.setParameter("lastname", lastname);
			return query.getSingleResult();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer getCustomerById(Integer id) throws DaoException {
		try {
			return em.find(Customer.class, id);
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createCustomer(Customer customer) throws DaoException {
		try {
			em.persist(customer);
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateCustomer(Customer customer) throws DaoException {
		try {
			Customer customerToUpdate = em.merge(customer);
			customerToUpdate.setFirstname(customer.getFirstname());
			customerToUpdate.setLastname(customer.getFirstname());
			customerToUpdate.setCompany(customer.getCompany());
			customerToUpdate.setMail(customer.getMail());
			customerToUpdate.setPhone(customer.getPhone());
			customerToUpdate.setMobile(customer.getMobile());
			customerToUpdate.setNotes(customer.getNotes());
			customerToUpdate.setActive(customer.isActive());
			customerToUpdate.setOrders(customer.getOrders());
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteCustomer(Customer customer) throws DaoException {
		try {
			em.remove(em.merge(customer));
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

}
